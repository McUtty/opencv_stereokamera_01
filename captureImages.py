import cv2
import numpy as np
import time
import sys
 
imageCount    = 15
interval    = 5 # seconds
outputPath    = "" # "images/"
filenameL    = "left%02d.jpg"
filenameR    = "right%02d.jpg"
 
if __name__ == '__main__':
    assert len(sys.argv) >= 3
 
    leftCamera  = int(sys.argv[1]) # 0 for "/dev/video0"
    rightCamera = int(sys.argv[2])
 
    if len(sys.argv) > 3: outputPath = sys.argv[3]
    if len(sys.argv) > 4: imageCount = int(sys.argv[4])
    if len(sys.argv) > 5: interval     = float(sys.argv[5])
 
    try:
        capL = cv2.VideoCapture(leftCamera)
        capR = cv2.VideoCapture(rightCamera)
        
        time.sleep(5) # wait some seconds
        
        for i in range(imageCount):
            print("Taking image %d:" % i)
            ret, frame = capL.read()
            cv2.imwrite(outputPath + filenameL % i, frame)
            print( "  Left image done." if ret else "  Error while taking left image...")
            ret, frame = capR.read()
            cv2.imwrite(outputPath + filenameR % i, frame)
            print( "  Right image done." if ret else "  Error while taking right image...")
            time.sleep(interval)
            
    finally:
        capL.release()
        capR.release()
        cv2.destroyAllWindows()